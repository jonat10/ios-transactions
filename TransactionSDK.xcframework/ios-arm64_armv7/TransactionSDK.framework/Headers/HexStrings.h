//
//  HexStrings.h
//  TransactionSDK
//
//  Created by Andres Ispani on 22/06/2020.
//  Copyright © 2020 GeoPagos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (HexString)

- (NSString *)hexString;

@end

@interface NSString (HexString)

- (NSData *)dataFromHex;

@end
